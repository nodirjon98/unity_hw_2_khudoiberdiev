using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game1
{

public class Game : MonoBehaviour
{
  public float lava_y_position = -1;
  public Unit hero;

  static public Game self;

  bool reset;

  void Awake()
  {
    self = this;
  }

  public void Reset()
  {
    reset = true;
  }

  void LateUpdate()
  {
    if(hero.transform.position.y < lava_y_position || reset)
    {
      reset = false;
      hero.Reset();
    }
  }
}

}
